FROM alpine:3.8 AS build
ENV PACKAGER "rene <rene@partidopirata.com.ar>"

RUN apk add --no-cache git postgresql-libs postgresql-dev tzdata alpine-sdk
RUN apk add --no-cache ruby-dev ruby-bundler ruby-json ruby-bigdecimal ruby-rake
# Crear una usuaria de trabajo, más que nada para que bundler no se
# queje que estamos corriendo como root
RUN addgroup -g 82 -S www-data
RUN adduser -s /bin/sh -G www-data -h /home/app -D app
RUN install -dm 2750 -o app -g www-data /home/app/web

USER app
WORKDIR /home/app/web
COPY --chown=app:www-data ./Gemfile .
COPY --chown=app:www-data ./Gemfile.lock .
# Instalar las gemas de producción
RUN bundle install --path=./vendor --without test development
# Eliminar cosas que ya no sirven
RUN rm vendor/ruby/2.5.0/cache/*.gem
# Limpiar las librerías nativas
RUN find vendor -name "*.so" | xargs -rn 1 strip --strip-unneeded
COPY --chown=app:www-data ./.git/ ./.git/
# Hacer un tarball de los archivos
RUN git archive -o ../app.tar.gz HEAD

FROM habitapp/monit:3.8
RUN apk add --no-cache postgresql-libs tzdata ruby ruby-bundler ruby-json ruby-bigdecimal ruby-rake
# Agregar el usuario y grupo
RUN addgroup -g 82 -S www-data
RUN adduser -s /bin/sh -G www-data -h /srv/http -D app
USER app
COPY --from=build --chown=app:www-data /home/app/app.tar.gz /tmp/
WORKDIR /srv/http
RUN tar xf /tmp/app.tar.gz && rm /tmp/app.tar.gz
# Traer las gemas
COPY --from=build --chown=app:www-data /home/app/web/vendor ./vendor
COPY --from=build --chown=app:www-data /home/app/web/.bundle ./.bundle
USER root
RUN install -m 640 -o root -g root ./monit.conf /etc/monit.d/puma.conf
EXPOSE 9292
