class CreatePositions < ActiveRecord::Migration[5.2]
  def change
    create_table :positions do |t|
      t.timestamps
      t.belongs_to :tracks, index: true
      t.float :speed
      t.float :accuracy
      t.timestamp :time
      t.string :provider
      t.geometry :coordinates, has_z: true, srid: 4326, geographic: true
    end
  end
end
