# Un grupo es una serie de tracks
class Grupo < ActiveRecord::Base
  has_many :tracks
  validates_uniqueness_of :name
end
