# Un modelo de recorrido
class Track < ActiveRecord::Base
  # Cada persona tiene muchos recorridos
  belongs_to :persona
  belongs_to :grupo
  has_many :positions
end
