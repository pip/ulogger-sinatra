# Un modelo de persona que registra datos
class Persona < ActiveRecord::Base
  # La contraseña va cifrada
  has_secure_password

  # Cada persona tiene muchos recorridos
  has_many :tracks

  # No se pueden repetir los nombres!
  validates_uniqueness_of :name

  before_create :random_color!

  def random_color!
    self.color = "#%06x" % (rand * 0xffffff)
  end
end
