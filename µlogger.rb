require 'sinatra'
require 'securerandom'
require 'time'

# ActiveRecord para trabajar con base de datos
require 'pg'
require 'activerecord-postgis-adapter'
require 'sinatra/activerecord'

# Modelos de la base de datos
require_relative 'app/models/persona'
require_relative 'app/models/grupo'
require_relative 'app/models/track'
require_relative 'app/models/position'

require 'rgeo/geo_json'

# Siempre aceptar conexiones desde cualquier IP
set :bind, '0.0.0.0'
# Usar cookies de sesión
enable :sessions
set :session_secret, SecureRandom.hex(64)

# La app µlogger envía datos a esta dirección
post '/:grupo/client/index.php' do
  # Le vamos a responder en JSON
  headers 'Content-Type' => 'application/json'
  # µlogger espera que le digamos si hay error o no
  res = { error: 'true' }

  case params[:action]
  when 'auth'
    # La primera vez que envía usuaria y contraseña, la crea y luego la
    # autentica, sino la patea
    persona = Persona.find_or_create_by(name: params[:user])

    # 
    if persona.new_record?
      persona.update_attribute :password, params['pass']
    end

    if persona.authenticate params['pass']
      res[:error] = 'false'
      session[:persona_id] = persona.id
    end
  # Crea un recorrido
  when 'addtrack'
    unless session[:persona_id]
      res[:message] = 'Unauthorized'
    else
      persona = Persona.find(session[:persona_id])
      grupo   = Grupo.find_or_create_by(name: params[:grupo])

      if persona
        track = persona.tracks.create(name: params[:track], grupo: grupo)
        res[:error] = 'false'
        res[:trackid] = track.id
      end
    end
  # Crea una posición
  when 'addpos'
    unless session[:persona_id]
      res[:message] = 'Unauthorized'
    else
      persona = Persona.find(session[:persona_id])
      track   = persona.tracks.find(params[:trackid])

      if track && coords = Position.hash_to_wkt(params)
        pos = track.positions.create(coordinates: coords,
          speed: params[:speed],
          time: Time.strptime(params[:time], '%s'),
          accuracy: params[:accuracy],
          provider: params[:provider])

        if pos.save
          res[:error] = 'false'
        end
      end
    end
  end

  # Respuesta en JSON
  body res.to_json
end

# Devolver todas las posiciones
get '/positions.geojson' do
  factory = RGeo::Cartesian.factory
  features = []

  grupo = Grupo.find_by(name: params[:grupo])
  if grupo
    grupo.tracks.includes(:persona, :positions).all.each do |t|
      line_string = factory.line_string(t.positions.map(&:coordinates))

      features << RGeo::GeoJSON::Feature.new(line_string, t.id, { color: t.persona.color })
      t.positions.each do |pos|
        features << RGeo::GeoJSON::Feature.new(pos.coordinates, pos.id, {
          persona: t.persona.name,
          track: t.name,
          time: pos.created_at,
          color: t.persona.color })
      end
    end
  end

  collection = RGeo::GeoJSON::FeatureCollection.new features

  headers 'Content-Type' => 'application/json'
  body RGeo::GeoJSON.encode(collection).to_json
end
